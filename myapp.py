# from kivy.app import App
# from kivy.uix.scatter import Scatter
# from kivy.uix.label import Label
# from kivy.uix.floatlayout import FloatLayout
# from kivy.uix.textinput import TextInput
# from kivy.uix.boxlayout import BoxLayout

# class ScatterTextWidget(BoxLayout):
#     pass

# class TutorialApp(App):
#     def build(self):
#         return ScatterTextWidget()

# if __name__ == "__main__":
#     TutorialApp().run()
#overall เหมือนกัน ต่างกันที่ structure

from kivy.app import App
from kivy.uix.scatter import Scatter
from kivy.uix.label import Label
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.textinput import TextInput
from kivy.uix.boxlayout import BoxLayout
import random

class ScatterTextWidget(BoxLayout):
    def change_label_color(self, *args):
        colour = [random.random() for i in range(3)] + [1]
        self.ids['my_label'].color = colour

class TutorialApp(App):
    def build(self):
        return ScatterTextWidget()

if __name__ == "__main__":
    TutorialApp().run()


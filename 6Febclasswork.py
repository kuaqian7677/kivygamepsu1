# //font_size = 150, text = Pongsatorn SaeFung ////<Ctrl K - > Ctrl C> ////< Ctrl K -> Ctrl U >
# import kivy
# from kivy.app import App 
# from kivy.uix.label import Label
# class MyApp (App):
#     def build(self):
#         return Label(text='Pongsatorn SaeFung', font_size=150)  # //ch.1
#     
# if __name__ == '__main__':
#     MyApp().run()

# from kivy.uix.floatlayout import FloatLayout
# from kivy.uix.scatter import Scatter
# from kivy.uix.label import Label

# class HelloApp(App):
#     def build(self):
#         f = FloatLayout()
#         s = Scatter()
#         l = Label(text='Hello!', font_size=150)
#         f.add_widget(s)
#         s.add_widget(l)

#         return f

# if __name__ == '__main__':
#     HelloApp().run() #//Ch 2

# from kivy.app import App
# from kivy.uix.scatter import Scatter
# from kivy.uix.label import Label
# from kivy.uix.floatlayout import FloatLayout
# from kivy.uix.boxlayout import BoxLayout
# from kivy.uix.textinput import TextInput

# class TutorialApp(App):
#     def build(self):
#         b = BoxLayout()
#         t = TextInput(font_size=150)
#         f = FloatLayout()
#         s = Scatter()
#         l = Label(text="Hello!", font_size=150)
#         f.add_widget(s)
#         s.add_widget(l)
#         b.add_widget(f)
#         b.add_widget(t)

#         return b

# if __name__ == "__main__":
#     TutorialApp().run()

# from kivy.app import App
# from kivy.uix.scatter import Scatter
# from kivy.uix.label import Label
# from kivy.uix.floatlayout import FloatLayout
# from kivy.uix.textinput import TextInput
# from kivy.uix.boxlayout import BoxLayout

# class TutorialApp(App):
#     def build(self):
#         b = BoxLayout(orientation='vertical')  #//orientation to vertical
#         t = TextInput(text='Hello', font_size=150, )  
#         l = Label(font_size=150)
#         b.add_widget(t)
#         b.add_widget(l)

#         t.bind(text=l.setter('text')) #here is it 

#         return b

# if __name__ == "__main__":
#     TutorialApp().run()

from kivy.app import App
from kivy.core.window import Window
from kivy.uix.scatter import Scatter
from kivy.uix.label import Label
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.textinput import TextInput
from kivy.uix.boxlayout import BoxLayout

class TutorialApp(App):
    def build(self):
        b = BoxLayout(orientation='vertical') #// set orient... = vert
        t = TextInput(text='Hello', font_size=150, size_hint_y=None, height=200)
        l = Label(font_size=150, text='Hello')
        f = FloatLayout()
        size = Window.size
        s = Scatter(pos=(90, size[1]/2 - 200)) #//set scatter and their starter pos
        f.add_widget(s)
        s.add_widget(l)
        b.add_widget(t)
        b.add_widget(f) #add that thing as widget inside b
        t.bind(text=l.setter('text')) # bind it 
        return b

if __name__ == "__main__":
    TutorialApp().run()


#here is how to create gui from kivy
#import kivy 
#create class inherit app class from kivy lib
    #class qowkepoma(App):
#create layout -> add logic,func,algo,..etc -> run it
#     def build(self):
#         return Button(text='Hello, Kivy!')

# if __name__ == '__main__':
#     MyApp().run()

